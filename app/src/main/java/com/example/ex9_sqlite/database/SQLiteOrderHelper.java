package com.example.ex9_sqlite.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.ex9_sqlite.model.Order;

import java.util.ArrayList;
import java.util.List;

public class SQLiteOrderHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "orderDB.db";
    private static final int DATABASE_VERSION = 1;
    public SQLiteOrderHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlCreateDatabase = "CREATE TABLE ordertable(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "itemName TEXT," +
                "dateOrder TEXT," +
                "price REAL," +
                "ratingOrder REAL)";
        sqLiteDatabase.execSQL(sqlCreateDatabase);
    }

    public long addOrder(Order order){
        ContentValues values = new ContentValues();
        values.put("itemName", order.getItemName());
        values.put("dateOrder", order.getDateOrder());
        values.put("price", order.getPrice());
        values.put("ratingOrder", order.getRatingOrder());
        SQLiteDatabase db = getWritableDatabase();
        return db.insert("ordertable",null, values);
    }

    public List<Order> getAll(){
        List<Order> list = new ArrayList<>();
        SQLiteDatabase db= getReadableDatabase();
        Cursor rs = db.query("ordertable",null, null, null, null, null, null);
        while ((rs != null) && (rs.moveToNext())){
            int id = rs.getInt(0);
            String itemName = rs.getString(1);
            String dateOrder = rs.getString(2);
            double price = rs.getDouble(3);
            float ratingOrder = rs.getFloat(4);
            list.add(new Order(id, itemName, dateOrder, price, ratingOrder));
        }
        rs.close();
        return list;
    }

    public List<Order> getOrderByItemName(String itemName){
        List<Order> list = new ArrayList<>();
        String whereClause = "itemName LIKE ?";
        String[] whereArgs = {"%" + itemName + "%"};
        SQLiteDatabase db= getReadableDatabase();
        Cursor rs = db.query("ordertable",null, whereClause, whereArgs, null, null, null);
        while ((rs != null) && (rs.moveToNext())){
            int id = rs.getInt(0);
            String itemname = rs.getString(1);
            String dateOrder = rs.getString(2);
            double price = rs.getDouble(3);
            float ratingOrder = rs.getFloat(4);
            list.add(new Order(id, itemname, dateOrder, price, ratingOrder));
        }
        rs.close();
        return list;
    }

    public int updateOrder(Order order){
        ContentValues values = new ContentValues();
        values.put("itemName", order.getItemName());
        values.put("dateOrder", order.getDateOrder());
        values.put("price", order.getPrice());
        values.put("ratingOrder", order.getRatingOrder());

        String whereClause = "id = ?";
        String[] whereArgs = {Integer.toString(order.getId())};
        SQLiteDatabase db = getWritableDatabase();
        return db.update("ordertable", values, whereClause, whereArgs);
    }

    public int deleteOrder(int id){
        String whereClause = "id = ?";
        String[] whereArgs = {Integer.toString(id)};
        SQLiteDatabase db = getWritableDatabase();
        return db.delete("ordertable", whereClause, whereArgs);
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
