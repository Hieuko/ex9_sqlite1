package com.example.ex9_sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ex9_sqlite.database.SQLiteOrderHelper;
import com.example.ex9_sqlite.model.Order;

import java.util.Calendar;

public class UpdateDeleteActivity extends AppCompatActivity {
    private Button btnUpdate, btnDelete, btnCacel, btnDate;
    private RatingBar rating;
    private EditText txtId, txtItemName, txtPrice;
    private TextView txtDateOrder;
    private SQLiteOrderHelper sqLiteOrderHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_delete);

        initView();

        Intent intent = getIntent();
        Order o = (Order) intent.getSerializableExtra("order");
        txtId.setText(o.getId() + "");
        txtItemName.setText(o.getItemName());
        txtDateOrder.setText(o.getDateOrder());
        txtPrice.setText(o.getPrice()+"");
        rating.setRating(o.getRatingOrder());
        txtId.setEnabled(false);

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mDay, mMonth, mYear;
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(UpdateDeleteActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        txtDateOrder.setText(i2 + "/" + (i1+1) + "/" + i);
                    }
                }, mYear, mMonth, mDay);
                dialog.show();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Order o = new Order();
                o.setId(Integer.parseInt(txtId.getText().toString()));
                o.setItemName(txtItemName.getText().toString());
                o.setDateOrder(txtDateOrder.getText().toString());
                o.setPrice(Double.parseDouble(txtPrice.getText().toString()));
                o.setRatingOrder(rating.getRating());
                long rs = sqLiteOrderHelper.updateOrder(o);
                if (rs > 0) Toast.makeText(UpdateDeleteActivity.this, "Update Order Successfull!", Toast.LENGTH_SHORT).show();
                else Toast.makeText(UpdateDeleteActivity.this, "Update Order Fail!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = Integer.parseInt(txtId.getText().toString());
                int rs = sqLiteOrderHelper.deleteOrder(id);
                if (rs > 0) Toast.makeText(UpdateDeleteActivity.this, "Delete Order Successfull!", Toast.LENGTH_SHORT).show();
                else Toast.makeText(UpdateDeleteActivity.this, "Delete Order Fail!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btnCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initView() {
        txtId = findViewById(R.id.txtId);
        txtItemName = findViewById(R.id.txtItemName);
        txtDateOrder = findViewById(R.id.txtDateOrder);
        txtPrice = findViewById(R.id.txtPrice);
        rating = findViewById(R.id.rating);

        btnUpdate = findViewById(R.id.btnUpdate);
        btnDelete = findViewById(R.id.btnDelete);
        btnCacel = findViewById(R.id.btnCancel);
        btnDate = findViewById(R.id.btnDate);

        sqLiteOrderHelper = new SQLiteOrderHelper(this);
    }
}