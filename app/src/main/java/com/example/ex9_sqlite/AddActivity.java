package com.example.ex9_sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ex9_sqlite.database.SQLiteOrderHelper;
import com.example.ex9_sqlite.model.Order;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity {
    private Button btnAdd, btnCacel, btnDate;
    private RatingBar rating;
    private EditText txtId, txtItemName, txtPrice;
    private TextView txtDateOrder;
    private SQLiteOrderHelper sqLiteOrderHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initView();

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mDay, mMonth, mYear;
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(AddActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        txtDateOrder.setText(i2 + "/" + (i1+1) + "/" + i);
                    }
                }, mYear, mMonth, mDay);
                dialog.show();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Order o = new Order();
                o.setItemName(txtItemName.getText().toString());
                o.setDateOrder(txtDateOrder.getText().toString());
                o.setPrice(Double.parseDouble(txtPrice.getText().toString()));
                o.setRatingOrder(rating.getRating());
                long rs = sqLiteOrderHelper.addOrder(o);
//                if (rs > 0) Toast.makeText(AddActivity.this, "Add Order Successfull!", Toast.LENGTH_SHORT).show();
//                else Toast.makeText(AddActivity.this, "Add Order Fail!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btnCacel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initView() {
        txtId = findViewById(R.id.txtId);
        txtItemName = findViewById(R.id.txtItemName);
        txtDateOrder = findViewById(R.id.txtDateOrder);
        txtPrice = findViewById(R.id.txtPrice);
        rating = findViewById(R.id.rating);

        btnAdd = findViewById(R.id.btnAdd);
        btnCacel = findViewById(R.id.btnCancel);
        btnDate = findViewById(R.id.btnDate);

        sqLiteOrderHelper = new SQLiteOrderHelper(this);
    }
}