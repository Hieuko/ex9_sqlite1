package com.example.ex9_sqlite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.ex9_sqlite.database.SQLiteOrderHelper;
import com.example.ex9_sqlite.model.Order;
import com.example.ex9_sqlite.model.RecyclerViewAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private FloatingActionButton btnAdd;
    private RecyclerView rv;
    private RecyclerViewAdapter adapter;
    private SQLiteOrderHelper sqLiteOrderHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.mSearch);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Order> list = sqLiteOrderHelper.getOrderByItemName(newText);
                adapter.setOrders(list);
                rv.setAdapter(adapter);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void initView() {
        btnAdd = findViewById(R.id.btnAdd);
        rv = findViewById(R.id.rv);

        adapter = new RecyclerViewAdapter();
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

        sqLiteOrderHelper = new SQLiteOrderHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Order> list = sqLiteOrderHelper.getAll();
        adapter.setOrders(list);
        rv.setAdapter(adapter);
    }
}