package com.example.ex9_sqlite.model;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ex9_sqlite.R;
import com.example.ex9_sqlite.UpdateDeleteActivity;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.OrderViewHolder> {
    private List<Order> list;

    public RecyclerViewAdapter() {
        list = new ArrayList<>();
    }

    public void setOrders(List<Order> list){
        this.list = list;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.cardview, parent, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order o = list.get(position);
        holder.txtId.setText(o.getId() + "");
        holder.txtName.setText(o.getItemName());
        holder.txtDate.setText(o.getDateOrder());
        holder.txtPrice.setText("" + o.getPrice());
        holder.rating.setRating(o.getRatingOrder());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.itemView.getContext(), UpdateDeleteActivity.class);
                Order o = list.get(position);
                intent.putExtra("order", o);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        } else
            return 0;
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        private TextView txtId, txtName, txtDate, txtPrice;
        private RatingBar rating;
        public OrderViewHolder(@NonNull View v) {
            super(v);
            txtId = v.findViewById(R.id.txtId);
            txtName = v.findViewById(R.id.txtName);
            txtDate = v.findViewById(R.id.txtDate);
            txtPrice = v.findViewById(R.id.txtPrice);
            rating = v.findViewById(R.id.rating);
        }
    }
}
